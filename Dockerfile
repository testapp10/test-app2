# This file is a template, and might need editing before it works on your project.
FROM node:latest

WORKDIR /usr/src/app

COPY package.json /usr/src/app/
RUN npm install

COPY . /usr/src/app

# replace this with your application's default port
EXPOSE 3000
CMD [ "node", "index.js" ]
